\chapter{Introduction}
\label{chp:INTRO}

%%%% Bullshit work around to get the numbering for figures and tables working on my Macbook %%%%
\renewcommand{\thefigure}{\thechapter.\arabic{figure}}
\renewcommand{\thetable}{\thechapter.\arabic{table}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Context}
\label{chp:INTRO:sec:CONTEXT}
The genesis of this work stemmed from frustration with the games I had been playing at the time I started this research. I enjoy first person shooter (FPS) games such as the Battlefield series \cite{battlefield} and the Call of Duty series \cite{callofduty}. The combination of reflex based play, where reaction time is key, and making strategic choices, for example knowing where to be to influence the game, is engrossing and challenging. Unfortunately the strategic component felt diminished, or missing, when I was interacting with characters not controlled by people.\\

I wanted a game experience where I felt like I was part of a team and one where the the opposing force reacted to what I was doing. What I got instead was a gaming experience that was split in two depending on the mode I chose to play. On the one hand there was the multiplayer experience that varied wildly according to the people involved. On the other hand was the single-player experience where you felt like you were forced down a "correct" linear path. In the multiplayer mode you could choose where to go, but in the single-player mode you were restricted and the opposing force always behaved in the same manner.\\

The restriction of choice was not a problem in and of itself. Shooting gallery games like Duck Hunt \cite{duckhunt}, Time Crisis \cite{timecrisis} and Aeon \cite{aeon} are fun and satisfying experiences because they do not promise more. Players have no control over where they position themselves. The skill is being fast enough to react to an unexpected situation and learning the timing and movement patterns of the enemies. The single-player experiences of Battlefield and Call of Duty were frustrating precisely because they seemed to promise more choice but didn't deliver on that promise.\\ 

I started by questioning why the game industry was not making the single-player games I craved. The first obstacle I identified was a perceived trend in consumer demand. Game publishers and developers believed that consumers wanted games that had built-in social and connected aspects. \citep{eafinished,Lee2008} Focusing development resources on social and/or multiplayer experiences would then be a natural reaction.\\

Another aspect was the economics of making games. A player wants to experience content and creating content for a game is expensive. \cite{Fahey2017} If the same content could be used over and over again to generate revenue then that would make the most economic sense. A multiplayer game, where the bulk of the player experience is via interaction with other players and content constitutes visual items that are reused, would have a larger return on investment than a single-player game where every interaction and visual element is created by a developer. Games like DOTA2 \cite{dota2} and League of Legends \cite{lol} are prime examples of successful games with little to no single-player aspect.\\

As an individual I could not influence game industry trends but I could investigate interventions that could make developing a game with a more complex single-player aspect more economically palatable. All FPS games have a visual aspect; thus visual content would always have to be made. With that in mind, I chose to focus on the artificial intelligence (AI). The games I was playing focused on a team of characters so a team-based game environment seemed apt. A source of frustration was how AI seemed to act the same way regardless of player action, so an AI that could consider the situation, choose a strategy and not be tied tightly to the environment seemed desirable. An AI that followed a recognisable strategy would seem more intelligent to a player. The AI being somewhat independent from the environment would mean that it could be reused in different environments. It also seemed desirable to use pre-existing AI if possible. Adding something that would make existing AI more interesting and compelling could save on development effort. The next step would be to investigate how to do all that.

\section{Idea Development}
In this section I will give an outline of how the final form of the research developed. I will touch on work that influenced my decision making while Chapter \ref{chp:LITREV} will consider the literature in more detail.\\

The basic elements of the AI agent that I was looking for seemed to be one that could function in a team, one that could decide on a strategy, one that was based on pre-existing AI and could be reusable in multiple environments. I felt that these elements balanced the strategic AI that I desired as opponents with the economic challenges that influenced game development.\\

I started with a textbook with the rather obvious title of "Artificial Intelligence for Games" by Ian Millington and John Funge \cite{Millington2009}. It was very useful as a reference on how techniques were used in difference game genres. This book was also where I first encountered reference to the game F.E.A.R. \cite{wikiFEAR} which was lauded for its AI upon release as well as twelve years later. \cite{Horti2017} The Game Developer's Conference paper describing the AI in F.E.A.R. \cite{Orkin2006} introduced me to the ideas of using planners in FPS games and using an AI responsible for strategy to coordinate the agents. Both the textbook and the paper \citep{Millington2009,Orkin2006} emphasised that, in the context of a game for entertainment, perceived intelligence was just as important as real intelligence. "Smoke and mirrors" is an effective and acceptable approach. In F.E.A.R. \cite{Orkin2006} group behaviour, such as flanking the player, was a result of designing the environment to provide multiple path choices for an AI agent when it needed to move to a better position, as opposed to a strategic choice to perform a flanking manoeuvre.\\

While looking into how to generate coordinated behaviour I came across a paper that described evolving coordination between agents using influence maps \cite{Avery2009}. While I decided not to pursue genetic algorithm based strategy creation, the idea of using influence maps for spatial reasoning stuck and became fundamental to the final AI agent created in this work.\\

While struggling to implement my own planner solution inspired by planning in F.E.A.R. I read a paper by Hoang et al. \cite{Hoang2005}. In \cite{Hoang2005} the authors described encoding strategy for FPS agents in a hierarchical task network (HTN). They used pre-existing agents modified to receive planned actions. The paper seemed to confirm that my idea of modifying existing AI agents had merit. From this point on HTN planning was the solution I would use for the strategic element of my final agent.\\

The final form of the AI in this work was a finite state machine (FSM) agent that could receive plans from a strategic coordinator that used influence maps to be spatially aware and an HTN to choose a plan of action. 

\section{Research Questions}
\label{chp:INTRO:sec:Questions}
The primary question for this research is "Does having a strategic layer improve the performance of simple agents and by how much?" The intuitive answer is yes. Adding a strategic layer would give the AI a way of selecting a strategy based on its current situation. Papers like \cite{Hoang2005} hint at the significance but do not quantify it because of the small number of game iterations used, specifically five games for a pair of competing AIs in that particular case.\\

Secondary questions stem from the primary question. The questions listed below would help to gauge where a game's AI development should focus: individually intelligent agents, agents that move in groups or coordinated agents.
\begin{itemize}
  \item Is there a situation where a team of uncoordinated agents performed consistently better than a coordinated team?
  \item How much do the individual agents matter?
  \item Intuitively a more complex individual should outperform a simpler one but is there a tipping point where complexity hinders performance? 
  \item Could a team of more complex agents working as individuals ever outperform a team of grouped but less complex agents? 
\end{itemize}

\section{Goals and Objectives}
\label{chp:INTRO:sec:OBJ}
The main goal would be to answer the primary question and measure the performance advantage, if any, of having a coordinator AI. To be able to measure the performance of the coordinator-guided agents there would need to be baseline agents against which they would compete and then be compared. To reliably quantify any performance difference, a significant number of games would have to be played. To test the reusability of the coordinator agents there would need to be multiple environments in which the AI could play games.\\

To achieve the goals software artefacts were needed. They are listed below.
\begin{itemize}
  \item A simulation environment in which the implemented AIs can act.
  \item Multiple maps/levels that change the topography of the environment to test reusability.
  \item Baseline AI agents to compare against.
  \item A coordinated AI agent that is based on a baseline agent.
  \item Enough test games so that meaningful conclusions can be made.
\end{itemize}

\section{Research Methodology}
The game implemented was played by two AIs paired off against each other. Define the AIs as A and B. The maps implemented had two starting locations. Two sets of matches were played for every AI pair. The first set, AB, was for AI A starting from the first starting location. The second set, BA, was for AI B starting from the first starting location. Every set comprised 2000 matches, thus 4000 matches were played for every AI pair.\\

Ten AIs were implemented and tested. The number of matches can be visualised as a ten by ten grid where each cell represented a set of 2000 matches, see Figure \ref{fig:introgrid}. When an AI played itself 4000 matches would be played. This meant that each possible AI pair played 4000 matches. This totals 220~000 matches played on a map. Three maps were implemented. The AIs are discussed later in this work and the abbreviations used in Figure \ref{fig:introgrid} are detailed in Table \ref{tab:airef}.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.9\linewidth]{figs/introGrid}
   \caption{Number of matches on a map visualised on an AI grid}
   \label{fig:introgrid}
\end{figure}

\section{Outline}
This chapter provided context for the work to follow. How the topic was chosen was described as well as the questions the work aims to answer. The remainder of the document proceeds as follows.\\

Chapter \ref{chp:LITREV} reviews relevant literature. Literature is discussed with reference to how it guided this work, how this work uses it, how this work builds upon it and how this work tries to answer unanswered questions. Chapter \ref{chp:THEORY} provides theoretical background for techniques used in the implementation of the simulation environment and AI agents developed for this work. Chapter \ref{chp:IMPL} describes the implementation of the simulation environment and the AI agents. Chapter \ref{chp:EXPERIMENT} describes the experiments performed and discusses the results of those experiments. Chapter \ref{chp:CONCLUSIONS} discusses the conclusions that can be drawn from the results of the experiments and discusses how closely the work achieved the objectives defined in this chapter.