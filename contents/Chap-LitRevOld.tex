\chapter{Literature Review OLD}
\label{chp:LITREVOLD}

work in progress (WIP) Chapter -- need to thoroughly check references\\

This chapter is a review of the sources touched during the research for this thesis. The chapter is structured to cover three themes. The first theme is individual artificial intelligence agent design methods, specifically the algorithms used to implement their behaviour selection. The second theme is group behaviour and how strategies for group coordination are formulated and structured. The third theme is tactical reasoning and spatial reasoning.\\

These themes dictated the structure and content of the research. Individual agents performed the actions within the game world. Their complexity contributed to the success of a given artificial intelligence iteration. Group behaviour and strategy added another layer to AI iterations. Tactical and spatial reasoning are fundamental to informing strategy and group behaviour.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Individual Agent Behaviour}
Fundamentally, algorithms that define the behaviour of an agent all take available knowledge and produce some action. \citep{Millington2009} In the context of games those actions usually are meant for the player to see, for example running to a position. This section starts with finite-state machines (FSM), continues with behaviours trees and concludes with goal-oriented action planning (GOAP).

\subsection{Finite-State Machines}
One such algorithm is a finite-state machine (FSM). A FSM is comprised of a finite set of discrete states and transitions. States represent an agent's current action, behaviour or demeanour. Only one state can be active at a time. Transitions define how and when the agent's active state changes.\\

Figure \ref{fig:lit-fsm} shows a FSM for an nervous man pacing between the door of his room and his bed. There are two states defining where he is pacing to and two transitions that take him back once he has reached a location.\\

\begin{figure}[!htbp]
   \centering
   \includegraphics[width=0.9\linewidth]{figs/lit/lit-FSM}
   \caption{Nervous man FSM}
   \label{fig:lit-fsm}
\end{figure}

Conceptually FSMs are intuitive, a state represents one action and the agent only changes action once a transition condition is met. This simplicity allows for easy implementation and visualisation, at least initially.\citep{Dawe2014}\\

A FSM can grow unwieldy quickly. Adding new states requires the addition of multiple transitions leading to, and away from, the new states. This leads to poor scalability and a situation known as "transition overload". An illustrative example would be if we added the behaviour state "Talk on phone" to the FSM is Figure \ref{fig:lit-fsm}. Conceptually this is a single state that gets triggered when a phone rings and once complete the nervous man would return to what he was doing. Unfortunately because the FSM has no memory mechanism for previous state this new state multiplies into two distinct states, one for talking while pacing to the door and another for talking while pacing to the bed. Adding that singular behaviour required two new states and four new transitions; doubling the number of states and tripling the number of transitions.[Champandard2007][Dawe2014]\\

To alleviate this structural limitation of FSMs hierarchical finite-state machines (HFSM) can be used. In an HFSM states are grouped into super-states. [hsfm] Super-states transition between one another but the previous state is stored as a history state [Dawe2014] and once the active state transitions back the history state is resumed.\\

These limitations make FSMs ideal if the desired behaviours can be divided into distinct states and if the number of states is small. [Nystrom]

\subsection{Behaviour Trees}
Another option for individual agent AI is a behaviour tree. Behaviour trees are made up of tasks and function as a depth-first search. The leaf tasks are conditions (some game world value test) and actions (something done by the agent to change the game world). The nodes of the tree are comprised of composite tasks. The two most common are selectors and sequences. Selectors select a child branch to explore. If a success result is returned it is passed to the selector's parent node. If a failure is returned the selector tries another child or passes failure up the tree. Sequences are a set of tasks to be performed in order. Sequences fail when one of their children fail. [Millington2009][Dawe2014][Champandard2008]\\

Behaviour trees are modular by design. The tasks are written independently of other tasks allowing designers to construct complex behaviours simply by creating trees out of tasks. New branches can be added to existing trees without overhead, alleviating the "transition overload" problem faced by finite-state machines. [Dawe2014]\\

The drawbacks of behaviour trees are that the tree must be run from the root node every time a decision has to be made. This approach can be slower than a FSM implementation depending on the depth to which the tree needs to be searched. Behaviour tree implementations can also be slow if there are many conditional checks within task preconditions, the cost of these checks will add up quickly the more complex the tree.

\subsection{Goal-Oriented Action Planning}
Goal-oriented action planning (GOAP) was most famously used in the game F.E.A.R. by Monolith releases in 2005. [Orkin2006][Wikipediab] Subsequent use of GOAP can be found in Just Cause 2 and Deus Ex: Human Revolution, released in 2010 and 2011 respectively. [Wikipedia][Wikipediaa] GOAP is based on a backward-chaining search planning algorithm called the Stanford Research Institute Problem Solver (STRIPS). [Fikes1971][Russell1995]\\

A STRIPS problem starts with a representation of the current world state, a representation of the goal / desired world state and a set of actions that can be performed. The world states are sets of predicates. An actions is defined by its preconditions (predicates that must be present for the action to be allowed) and its effects. The effects consist of an add list (predicates added to the world state by performing the action) and the delete list (predicates removes from the world state by this action). The STRIPS algorithm then works from the goal state backwards matching effects and resolving conflicts until the current world state is recreated. The resulting set of actions is the plan the agent will try to carry out.\\

GOAP differs from STRIPS in a few ways. Actions in GOAP have a cost. This allows actions to have a priority and the use of a A* like search through state-space for a solution. Actions do not have add or delete lists, rather arrays representing the resulting world state are used. Procedural preconditions and procedural effects are used. This allows doing costly calculations, like pathfinding or line-of-sight checks, to be evaluated when the plan is actioned instead of during planning when all it would do would be slow the planner. [Orkin2006]\\

The advantage of this style of agent design is that agent actions are simple reusable elements that are constructed into behaviours by the planning algorithm rather than the game designers. This allows unthought of, and potentially novel, solutions to be found. The disadvantages are that the planner must be able to plan quickly. GOAP has no way to define external effects on the world state thus must be able to formulate a plan when the one being carried out, or being calculated, is invalidated. For example when an agent tries its plan of shooting the player but the player blocks it with a special ability. The planner must then make a new plan taking this ability into account. The planner must then also be able to perform these actions for every agent in the game world. Another disadvantage is that, if not coded for specifically, designers cannot use GOAP agents in a cutscene where something specific most take place. This causes the loss of narrative control and require special coding or cutscene production.

\section{Group Behaviour and  Strategy}
Coordination is a hallmark of intelligence thus in games agents that seem to coordinate with one another seem far more capable than those that seem to work opaquely toward their own goals. There are two broad forms of coordination. The first is emergent coordination. This form relieves on a system or algorithm to produce cooperative, and mutually beneficial, behaviour without a human deciding what to do. The second form of cooperation is designed or prescribed cooperation. This is when a human designer dictates how a group of agents should behave in concert to produce intelligent behaviour. This most often takes a multi-tier top down approach that mimics military hierarchy. [Millington2009] ch6\\

\subsection{Emergent}
Emergent group behaviour can be achieved by automatically tuning the agents' decision making algorithms to be more effective at team based games. The authors of [Liaw2013] took the agents in Quake 3, superimposed a FSM representing game state in a capture-the-flag game and tuned the rule based system in the standard agent via genetic algorithm. They improved the results of a group of evolved agents playing against the standard agents.\\

Similar work has been done for Unreal Tournament 2004 in [Mora2010]. The authors took a set of parameters from the game's HFSM and used a genetic algorithm to tune the values. The goal was to improve the agent performance. The chosen game mode was team-deathmatch. Because of the chosen game mode the the fitness function used was weighted to favour number of points scored (number of enemies killed). Number of deaths was the only attribute that lowered the fitness value.\\

In [Liaw2013] and [Mora2010] the approach taken to improving group behaviour of the agents was to improve singular agent decision making, via genetic algorithms, in the context of a group objective and both were successful in doing so. One drawback to this approach is that it takes time to run the necessary cycles to evolve the agents, which can be mitigated if the game speed can be accelerated. A second drawback is that this approach requires that there be an existing intelligence to play against and to improve upon. This means that the initial work of creating an AI has to be done and then the improvement can begin. Genetic algorithms will not necessarily lead to a good AI. They will produce an improved AI but the opponent is a limiting factor. If the opponent is bad the evolved AI is a bit better than a bad AI. You would have to continually evolve to produce a good one which can take time.
From a player perspective GA improvements to an agent are opaque. If the GA improvement leads to counterintuitive actions, even if beneficial, players may simply assume the agent is stupid. [[this bit needs phrase work]]
 [[Does an excessively good opponent hamper improvement via GA??]]\\
 
 Another approach to using genetic algorithms to create group behaviour is shown in [Avery2009]. The approach here is not to evolve agent decision making but to evolve the route they take to an objective. What was done was to evolve a set of influence maps that define agent objectives. Once one objective was reached the next influence map in the set would be used to define the next objective.\\

This approach produced a number of group tactics such as flanking and sacrificing a member to achieve the goal. While able to produce successful tactics this approach requires that the situation be static. The team objective never moved nor did the enemies. In games this is rarely the case. On a large scale this approach may not be successful but on a small scale is could be. If the game could be broken down into multiple small scenarios, thus limiting the number of possible enemy positions, a playbook of sorts could be constructed. This would require an overseeing commander AI to decide the matching scenario and assign objectives to the agents.

\subsection{Prescribed}
Instead of generating group behaviour, as in the previous section, it can be explicitly defined by a designer. In [Hoang2005] the authors took the standard agents in Unreal Tournament 2004 and placed an AI above them. This overseeing AI defined what the objective of each agent would be but did not prescribe how they interacted with the world.\\

Group behaviour - police restrain - [Gorniak2007]

\subsection{Combined}
The AI used in F.E.A.R. [Orkin2006] is an example of combining emergent and prescribed behaviour to produce group coordination. As stated in Section 1, the individual agents in FEAR use GOAP to assemble action plans to fulfill their goals. The agents were also organised into squads by a "global coordinator" AI. This coordinator would group agents that were near each other together and give them one of four behaviours that prescribed how the agents moved as a group. These squad orders were added to the agent's goals and the agent was left to decide on the priority of following that goal vs the general goals.\\

One such prescribed behaviour is "Advance-Cover", this prescribes a movement pattern of one squad member moving to a forward cover position while a second supplied protective covering fire. Unscripted behaviour emerged from this. An example was an advancing agent flanking the player because the next available free position was next to the player. Flanking was not a programmed behaviour but emerged for the perceived team work of a flanking agent and their cover fire squadmate.\\

This synergy between GOAP and prescribed behaviour is a great advantage to AI designers who want to create agents that can surprise even them. It automatically adds depth to the game. The disadvantage is that a system like GOAP must be built or already be in place, bringing with it its own technical overhead and lost of narrative control.

\subsection{Tactical and Spatial Reasoning}

\section{Learning}
neural networks are a form of learning. the weights change based on feedback.

reinforcement learning needs to be discussed here. why didn't i take this approach? because there was an advantage to having prescribed behaviour from the designer's perspective. need to relate that the set up of reinforcement learning would also be a challenge on it's own. finding policies. finding data. flesh this out once I've read more on the theory and methods.

reinforcement learning...

\section{Conclusion}
Need game examples for FSM, HFSM and BTs. Paris conference!
BT diagrams would help.
HTN for individual agents
Break [Millington2009] refs into chapters -- might not need to do this.\\

Strategy evolved using influence maps valid for static scenario. too closely linked to the influence map and specific scenario to be general enough.

A commander AI could be used to engineer scenarios for agents to react in. We have good ways to make reactive agents (fsm and behaviour trees). Agents simply need to be placed to good positions to react.
