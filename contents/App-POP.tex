\chapter{Worked Partial-Order Planning Example}
\label{chp:APP-POP}

This the full working of the shopping world world example in Section \ref{chp:THEORY:sec:Planning:sub:pop}. The premise in natural language is that we are at home and would like to go shopping, buy three items and return home. Those items are bread, milk and a hammer. We know that ShopA sells hammers and that ShopB sells bread and milk.\\

\begin{lstlisting}[caption={Shopping Example Operators},captionpos=b,label={lst:app-pop:operators},mathescape=true,frame=single]
Op[ACTION:Start,
  EFFECT: At(Home) $\wedge$ Sells(ShopA,Hammer) 
  $\wedge$ Sells(ShopB,Bread) $\wedge$ Sells(ShopB,Milk)]

Op[ACTION:Finish,
  PRECOND: Have(Hammer) $\wedge$ Have(Bread) $\wedge$ Have(Milk)
   $\wedge$ At(Home)]

Op[ACTION:Go(there),
  PRECOND: At(here),
  EFFECT: At(there) $\wedge$ $\neg$At(here)]

Op[ACTION:Buy(x),
  PRECOND: At(y) $\wedge$ Sells(y,x),
  EFFECT: Have(x)]
\end{lstlisting}

There are four valid operators. They are \texttt{Op[Start]}, \texttt{ Op[Finish]},  \texttt{Op[Buy(x)]} and \texttt{Op[Go(there)]}. The modified STRIPS format from \cite{Russell1995} for the operators is given in Listing \ref{lst:app-pop:operators}. Operator \texttt{Op[Start]}, describes the world state at the beginning, we are at home, and describes our knowledge of the world, in this case where items can be purchased. Operator \texttt{ Op[Finish]} defines how we would like the world state to be after we have gone shopping, ie being at home and having the three items in our possession. Operators \texttt{Op[Go(there)]} and  \texttt{Op[Buy(x)]} describe actions we can take and list the preconditions for those actions to be valid and the effects taking those actions have on the world state.\\

The algorithm starts with a minimal plan consisting of our starting state and the finishing state, see Listing \ref{lst:app-pop:minimal}. We then follow a series of steps, listed below, and repeat as necessary to find a series of steps with actions that if followed take us from the initial world state to the desired final world state.\\

The steps are as follows:
\begin{enumerate}
 \item Check if the current plan, \texttt{PLAN}, is a solution. If not, continue.
 \item Select a step from \texttt{STEPS{}} that has an unachieved precondition, $S_{need}$ and $c$ respectively.
 \item Select a pre-existing step or new operator, $S_{add}$, that has an effect that can achieve precondition $c$.
   \begin{itemize}
   \item If there are no valid steps or operators this plan is a failure. Either backtrack to a previous plan or there is no possible solution.
   \item A valid choice does three things:
   	\begin{itemize}
   		\item It adds three items to \texttt{ORDERING{}}. The first states that step $S_{add}$ must precede step $S_{need}$, shown as \mbox{$S_{add} \prec S_{need}$}. The second states that the start step, $S_1$, must precede step $S_{add}$, \mbox{$S_{1} \prec S_{add}$}. The third states that step $S_{add}$ must precede the finish step, $S_{2}$, \mbox{$S_{add} \prec S_2$}.
   		\item It adds an item to \texttt{LINKS{}} saying that step $S_{add}$ achieves the precondition $c$ of step $S_{need}$, shown as \mbox{$S_{add} \overset{c}{\rightarrow} S_{need}$}.
   		\item It adds step $S_{add}$ to the list \texttt{STEPS}.
	\end{itemize}   	   
   \end{itemize}
 \item Try to resolve threats to links. A threat is a step, $S_{threat}$, that has the effect of negating, $\neg c$, a defined link $S_i \overset{c}{\rightarrow} S_j$. We try to resolve the threat by either ordering $S_{threat}$ before $S_i$, $S_{threat} \prec S_i$, or by ordering $S_{threat}$ after $S_j$, $S_j \prec S_{threat}$. If the plan has no ordering or variable binding contradictions we consider it consistent and continue the algorithm.
\end{enumerate}

\pagebreak

\begin{lstlisting}[caption={Minimal Plan},captionpos=b,label={lst:app-pop:minimal},mathescape=true,frame=single]
PLAN [
STEPS: {
$S_1$: Op[ACTION:Start,
    EFFECT: At(Home) $\wedge$ Sells(ShopA,Hammer) 
  $\wedge$ Sells(ShopB,Bread) $\wedge$ Sells(ShopB,Milk)],
$S_2$: Op[ACTION:Finish,
    PRECOND: Have(Hammer) $\wedge$ Have(Bread) $\wedge$ Have(Milk)
   $\wedge$ At(Home)]},
ORDERINGS: {$S_1 \prec S_2$},
BINDINGS: {},
LINKS: {}]
\end{lstlisting}

Now we will produce a solution for the problem by following the algorithm steps. The incomplete plans produced at each stage will be shown as listings. The start and finish steps, $S_1$ and $S_2$, will be abbreviated to just those symbols because they do not change. The \texttt{BINDINGS} list will be omitted in favour of replacing the the variable in the operators with values. For example if we use the operator \texttt{Op[Buy(x)]} to buy \texttt{Milk} we won't show that there is a binding $x = Milk$ but we will fill in the variables in the \texttt{Op[Buy(Milk)]} step in the plan.\\

The current plan, Listing \ref{lst:app-pop:minimal}, is not a solution so we select an unachieved precondition. Let's choose \texttt{Have(Hammer)} from step $S_2$. The only operator that can achieve the \texttt{Have(Hammer)} condition is \texttt{Op[Buy(x)]} with $x = Hammer$. Listing \ref{lst:app-pop:s3} shows the plan produced from this iteration.\\

\begin{lstlisting}[caption={Step 3},captionpos=b,label={lst:app-pop:s3},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At(y) $\wedge$ Sells(y,Hammer),
  EFFECT: Have(Hammer)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$},
LINKS: {$S_3 \overset{Have(Hammer)}{\rightarrow} S_2$}]
\end{lstlisting}

If for the next two iterations of the algorithm we choose the $S_2$ preconditions \texttt{Have(Bread)} and \texttt{Have(Milk)} we will get the plan shown in Listing \ref{lst:app-pop:s4s5}. There are no threatening steps in the plan yet because there are no negating affects thus the actions don't interfere with one another.

\pagebreak

\begin{lstlisting}[caption={Step 4 and Step 5},captionpos=b,label={lst:app-pop:s4s5},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At($y_3$) $\wedge$ Sells($y_3$,Hammer),
  EFFECT: Have(Hammer)],
$S_4$: Op[ACTION:Buy(Bread),
  PRECOND: At($y_4$) $\wedge$ Sells($y_4$,Bread),
  EFFECT: Have(Bread)],
$S_5$: Op[ACTION:Buy(Milk),
  PRECOND: At($y_5$) $\wedge$ Sells($y_5$,Milk),
  EFFECT: Have(Milk)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$, $S_1 \prec S_4$, $S_4 \prec S_2$, 
$S_1 \prec S_5$, $S_5 \prec S_2$},
LINKS: {($S_3 \overset{Have(Hammer)}{\rightarrow} S_2$), ($S_4 \overset{Have(Bread)}{\rightarrow} S_2$), 
($S_5 \overset{Have(Milk)}{\rightarrow} S_2$)}]
\end{lstlisting}

Assume for the next three iterations of the algorithm we choose the preconditions $S_3: Sells(y_3,Hammer)$, $S_4: Sells(y_4,Bread)$ and $S_5: Sells(y_5,Milk)$ respectively. Step $S_1$ achieves all three of these preconditions because knowledge of where the items are sold is part of the initial world state as defined in operator \texttt{Op[Start]}. Through defining the values of $y_3$, $y_4$ and $y_5$ we better define the \texttt{At(x)} preconditions seen in steps $S_3$, $S_4$ and $S_5$. Listing \ref{lst:app-pop:sells} shows the resulting plan.

\begin{lstlisting}[caption={Sells(y,x) Preconditions},captionpos=b,label={lst:app-pop:sells},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At(ShopA) $\wedge$ Sells(ShopA,Hammer),
  EFFECT: Have(Hammer)],
$S_4$: Op[ACTION:Buy(Bread),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Bread),
  EFFECT: Have(Bread)],
$S_5$: Op[ACTION:Buy(Milk),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Milk),
  EFFECT: Have(Milk)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$, $S_1 \prec S_4$, $S_4 \prec S_2$, 
$S_1 \prec S_5$, $S_5 \prec S_2$},
LINKS: {($S_3 \overset{Have(Hammer)}{\rightarrow} S_2$), ($S_4 \overset{Have(Bread)}{\rightarrow} S_2$), 
($S_5 \overset{Have(Milk)}{\rightarrow} S_2$),($S_1 \overset{Sells(ShopA,Hammer)}{\rightarrow} S_3$),($S_1 \overset{Sells(ShopB,Bread)}{\rightarrow} S_4$),
($S_1 \overset{Sells(ShopB,Milk)}{\rightarrow} S_5$)}]
\end{lstlisting}

Assume for the next three algorithm iterations we choose to achieve the preconditions $S_3: At(ShopA)$, $S_4: At(ShopB)$ and $S_5: At(ShopB)$. Adding the operator \texttt{Op[Go(ShopA)]} as step $S_6$ would achieve $S_3: At(ShopA)$. Adding the operator \texttt{Op[Go(ShopB)]} as step $S_7$ would achieve $S_4: At(ShopB)$. On the third iteration we would link the effect of $S_7$ to achieve $S_5: At(ShopB)$. No threats are generated because the negation effects of the \texttt{Op[Go(x)]} operators refer to variables and not actual links. The plan so far can be seen in Listing \ref{lst:app-pop:gogox}.\\

\begin{lstlisting}[caption={After adding two Go(x) operators.},captionpos=b,label={lst:app-pop:gogox},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At(ShopA) $\wedge$ Sells(ShopA,Hammer),
  EFFECT: Have(Hammer)],
$S_4$: Op[ACTION:Buy(Bread),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Bread),
  EFFECT: Have(Bread)],
$S_5$: Op[ACTION:Buy(Milk),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Milk),
  EFFECT: Have(Milk)], 
$S_6$: Op[ACTION:Go(ShopA),
  PRECOND: At($here_6$),
  EFFECT: At(ShopA) $\wedge$ $\neg$At($here_6$)],
$S_7$: Op[ACTION:Go(ShopB),
  PRECOND: At($here_7$),
  EFFECT: At(ShopB) $\wedge$ $\neg$At($here_7$)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$, $S_1 \prec S_4$, $S_4 \prec S_2$, 
$S_1 \prec S_5$, $S_5 \prec S_2$, $S_6 \prec S_3$, $S_1 \prec S_6$, $S_6 \prec S_2$, $S_7 \prec S_4$, $S_1 \prec S_7$, 
$S_7 \prec S_2$, $S_7 \prec S_5$},
LINKS: {($S_3 \overset{Have(Hammer)}{\rightarrow} S_2$), ($S_4 \overset{Have(Bread)}{\rightarrow} S_2$), 
($S_5 \overset{Have(Milk)}{\rightarrow} S_2$),($S_1 \overset{Sells(ShopA,Hammer)}{\rightarrow} S_3$),($S_1 \overset{Sells(ShopB,Bread)}{\rightarrow} S_4$),
($S_1 \overset{Sells(ShopB,Milk)}{\rightarrow} S_5$), ($S_6 \overset{At(ShopA)}{\rightarrow} S_3$), ($S_7 \overset{At(ShopB)}{\rightarrow} S_4$), 
($S_7 \overset{At(ShopB)}{\rightarrow} S_5$)}]
\end{lstlisting}

Now we describe a sequence of iterations that lead to the algorithm to back track after find a threat it can't resolve. Firstly we choose the precondition $S_6: At(here_6)$ to be achieved. This can be achieved by linking the precondition to the \texttt{At(Home)} effect of $S_1$. This introduces the link $S_1 \overset{At(Home)}{\rightarrow} S_6$. On the next iteration we choose to achieve precondition $S_7: At(here_7)$ by linking it to $S_1$ as in the previous iteration. This introduces threats to links. $S_7$ now has the effect $\neg At(Home)$ which threatens the link $S_1 \overset{At(Home)}{\rightarrow} S_6$ introduced in the previous iteration. The threat resolution methods of changing orderings will lead to inconsistencies because $S_7$ can't happen before $S_1$ by definition and ordering $S_7$ after $S_6$ while it is linked to $S_1$ would lead to the need for $S_1$ to happen after $S_6$, also impossible. This is where the algorithm recognises an inconsistent plan and backtracks to a previous plan knowing it can't make that decision again.\\

Now assume that after backtracking the algorithm tries to achieve the same precondition but with the \texttt{At(ShopA)} effect of $S_6$. This now threatens the link ($S_6 \overset{At(ShopA)}{\rightarrow} S_3$) because $S_7$ now has the effect $\neg At(ShopA)$. This threat is resolved by requiring $S_3$ to precede $S_7$, $S_3 \prec S_7$. The resulting plan can be seen in Listing \ref{lst:app-pop:backtrack}.\\

\begin{lstlisting}[caption={After backtracking.},captionpos=b,label={lst:app-pop:backtrack},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At(ShopA) $\wedge$ Sells(ShopA,Hammer),
  EFFECT: Have(Hammer)],
$S_4$: Op[ACTION:Buy(Bread),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Bread),
  EFFECT: Have(Bread)],
$S_5$: Op[ACTION:Buy(Milk),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Milk),
  EFFECT: Have(Milk)], 
$S_6$: Op[ACTION:Go(ShopA),
  PRECOND: At(Home),
  EFFECT: At(ShopA) $\wedge$ $\neg$At(Home)],
$S_7$: Op[ACTION:Go(ShopB),
  PRECOND: At(ShopA),
  EFFECT: At(ShopB) $\wedge$ $\neg$At(ShopA)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$, $S_1 \prec S_4$, $S_4 \prec S_2$, 
$S_1 \prec S_5$, $S_5 \prec S_2$, $S_6 \prec S_3$, $S_1 \prec S_6$, $S_6 \prec S_2$, $S_7 \prec S_4$, $S_1 \prec S_7$, 
$S_7 \prec S_2$, $S_7 \prec S_4$, $S_6 \prec S_7$, $S_3 \prec S_7$},
LINKS: {($S_3 \overset{Have(Hammer)}{\rightarrow} S_2$),($S_4 \overset{Have(Bread)}{\rightarrow} S_2$),
($S_5 \overset{Have(Milk)}{\rightarrow} S_2$),($S_1 \overset{Sells(ShopA,Hammer)}{\rightarrow} S_3$),($S_1 \overset{Sells(ShopB,Bread)}{\rightarrow} S_4$),
($S_1 \overset{Sells(ShopB,Milk)}{\rightarrow} S_5$),($S_6 \overset{At(ShopA)}{\rightarrow} S_3$),($S_7 \overset{At(ShopB)}{\rightarrow} S_4$),
($S_7 \overset{At(ShopB)}{\rightarrow} S_5$),($S_1 \overset{At(Home)}{\rightarrow} S_6$),($S_6 \overset{At(ShopA)}{\rightarrow} S_7$)}]
\end{lstlisting}

The next step in finding a solution is to achieve the \texttt{At(Home)} precondition of step $S_2$. We can achieve this by adding a \texttt{Op[Go(there)]} operator as step $S_8$, ($S_8 \overset{At(Home)}{\rightarrow} S_2$), but this link is threatened by the $S_6$ effect $\neg At(Home)$ so we resolve it by saying $S_6$ must precede $S_8$. In the next iteration we need to achieve precondition $S_8: At(here_8)$. We can't use $S_6$ to create $S_8: At(ShopA)$ because $S_7$ would pose an unresolvable threat. We then use $S_7$ to create $S_8: At(ShopB)$. This leads to threats to the \texttt{At(ShopB)} conditions of steps $S_4$ and $S_5$ but this can be resolved by ordering $S_8$ after them. The final plan can be seen in Listing \ref{lst:app-pop:final}.\\

\begin{lstlisting}[caption={Final Plan},captionpos=b,label={lst:app-pop:final},mathescape=true,frame=single]
PLAN [
STEPS: {$S_1$,$S_2$,
$S_3$: Op[ACTION:Buy(Hammer),
  PRECOND: At(ShopA) $\wedge$ Sells(ShopA,Hammer),
  EFFECT: Have(Hammer)],
$S_4$: Op[ACTION:Buy(Bread),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Bread),
  EFFECT: Have(Bread)],
$S_5$: Op[ACTION:Buy(Milk),
  PRECOND: At(ShopB) $\wedge$ Sells(ShopB,Milk),
  EFFECT: Have(Milk)], 
$S_6$: Op[ACTION:Go(ShopA),
  PRECOND: At(Home),
  EFFECT: At(ShopA) $\wedge$ $\neg$At(Home)],
$S_7$: Op[ACTION:Go(ShopB),
  PRECOND: At(ShopA),
  EFFECT: At(ShopB) $\wedge$ $\neg$At(ShopA)],
$S_8$: Op[ACTION:Go(Home),
  PRECOND: At(ShopB),
  EFFECT: At(Home) $\wedge$ $\neg$At(ShopB)]},
ORDERINGS: {$S_1 \prec S_2$, $S_1 \prec S_3$, $S_3 \prec S_2$, $S_1 \prec S_4$, $S_4 \prec S_2$, 
$S_1 \prec S_5$, $S_5 \prec S_2$, $S_6 \prec S_3$, $S_1 \prec S_6$, $S_6 \prec S_2$, $S_7 \prec S_4$, $S_1 \prec S_7$, 
$S_7 \prec S_2$, $S_7 \prec S_5$, $S_6 \prec S_7$, $S_3 \prec S_7$, $S_1 \prec S_8$, $S_8 \prec S_2$, $S_6 \prec S_8$, 
$S_7 \prec S_8$, $S_4 \prec S_8$, $S_5 \prec S_8$},
LINKS: {($S_3 \overset{Have(Hammer)}{\rightarrow} S_2$),($S_4 \overset{Have(Bread)}{\rightarrow} S_2$),
($S_5 \overset{Have(Milk)}{\rightarrow} S_2$),($S_1 \overset{Sells(ShopA,Hammer)}{\rightarrow} S_3$),($S_1 \overset{Sells(ShopB,Bread)}{\rightarrow} S_4$),
($S_1 \overset{Sells(ShopB,Milk)}{\rightarrow} S_5$),($S_6 \overset{At(ShopA)}{\rightarrow} S_3$),($S_7 \overset{At(ShopB)}{\rightarrow} S_4$),
($S_7 \overset{At(ShopB)}{\rightarrow} S_5$),($S_1 \overset{At(Home)}{\rightarrow} S_6$),($S_6 \overset{At(ShopA)}{\rightarrow} S_7$),($S_8 \overset{At(Home)}{\rightarrow} S_2$),
($S_7 \overset{At(ShopB)}{\rightarrow} S_8$)}]
\end{lstlisting}